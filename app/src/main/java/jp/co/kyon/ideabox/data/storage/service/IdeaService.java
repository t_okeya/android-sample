package jp.co.kyon.ideabox.data.storage.service;

import android.app.Activity;
import android.content.Context;

import jp.co.kyon.ideabox.App;
import jp.co.kyon.ideabox.business.entities.greendao.IdeaEntity;
import jp.co.kyon.ideabox.business.entities.greendao.IdeaEntityDao;

/**
 * <code>Idea</code>サービスクラスです.<br>
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IdeaService  {

    /** . */
    private Activity target = null;

    /**
     * コンストラクタです.
     */
    public IdeaService(Context context) {
        this.target = (Activity) context;
    }

    public void add(IdeaEntity entity) {
        IdeaEntityDao ideaDao = ((App)this.target.getApplication()).getDaoSession().getIdeaEntityDao();
        ideaDao.insert(entity);
    }

}
