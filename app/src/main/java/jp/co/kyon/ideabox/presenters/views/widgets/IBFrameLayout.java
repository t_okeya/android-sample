package jp.co.kyon.ideabox.presenters.views.widgets;

import android.content.Context;
import android.widget.FrameLayout;

/**
 * <code>android.widget.FrameLayout</code>拡張クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IBFrameLayout extends FrameLayout {

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBFrameLayout(Context context) {
        super(context);
    }
}
