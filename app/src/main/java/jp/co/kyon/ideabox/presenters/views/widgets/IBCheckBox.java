package jp.co.kyon.ideabox.presenters.views.widgets;

import android.content.Context;
import android.support.v7.widget.AppCompatCheckBox;

/**
 * <code>android.support.v7.widget.AppCompatCheckBox</code>拡張クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IBCheckBox extends AppCompatCheckBox {

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBCheckBox(Context context) {
        super(context);
    }

}
