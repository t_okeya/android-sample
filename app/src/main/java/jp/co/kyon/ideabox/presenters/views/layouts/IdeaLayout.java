package jp.co.kyon.ideabox.presenters.views.layouts;

import android.content.Context;
import android.graphics.Typeface;

import com.rengwuxian.materialedittext.MaterialEditText;
import com.rey.material.widget.EditText;

import jp.co.kyon.ideabox.presenters.views.widgets.IBEditText;
import jp.co.kyon.ideabox.presenters.views.widgets.IBLinearLayout;

/**
 * <code>IdeaLayout</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IdeaLayout extends IBLinearLayout {

    private IBEditText ideaEditText = null;

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IdeaLayout(Context context) {
        super(context);
        setOrientation(IBLinearLayout.HORIZONTAL);
    }

    public void init(int text, int hint, int editTextId) {

        this.ideaEditText = new IBEditText(getTarget());
        this.ideaEditText.setLayoutParams(
                new IBLinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        this.ideaEditText.setId(editTextId);
        this.ideaEditText.setHint(hint);
        this.ideaEditText.setFloatingLabel(MaterialEditText.FLOATING_LABEL_HIGHLIGHT);
        this.ideaEditText.setFloatingLabelText(getContext().getResources().getText(text));
        super.addView(this.ideaEditText);

        EditText editText = new EditText(getTarget());
//        editText.setLayoutParams(
//                new IBLinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
//        editText.setId(editTextId);
//        editText.setHint(hint);
//        editText.applyStyle(EditText.SUPPORT_MODE_HELPER);
//        super.addView(editText);
    }

    public void setFont(Typeface face) {
        this.ideaEditText.setTypeface(face);
    }

    public String getText4EditText() {
        return this.ideaEditText.getText().toString();
    }

    public void setText4EditText(String text) {
        this.ideaEditText.setText(text);
    }

    public void clearText4EditText() {
        this.ideaEditText.setText("");
    }

}
