package jp.co.kyon.ideabox.presenters.views.widgets;

import android.app.Activity;
import android.content.Context;
import android.widget.RelativeLayout;

/**
 * <code>android.widget.RelativeLayout</code>拡張クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IBRelativeLayout extends RelativeLayout {

    /** 対象の{@link Activity}. */
    private Activity target = null;

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBRelativeLayout(Context context) {
        super(context);
        this.target = (Activity) context;
    }

    /**
     * 対象の{@link Activity}を返却します.
     *
     * @return {@link Activity}
     */
    public Activity getTarget() {
        return this.target;
    }

}
