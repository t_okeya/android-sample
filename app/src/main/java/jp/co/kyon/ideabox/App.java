package jp.co.kyon.ideabox;

import android.app.Application;

import org.greenrobot.greendao.database.Database;

import jp.co.kyon.ideabox.business.entities.greendao.DaoMaster;
import jp.co.kyon.ideabox.business.entities.greendao.DaoSession;

/**
 * サービスの抽象クラスです.<br>
 * サービスクラスを実装する際は、当クラスを継承してください.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class App extends Application {

    /** . */
    private DaoSession daoSession = null;

    @Override
    public void onCreate() {
        super.onCreate();

        DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "idea_box_db");
        Database db = helper.getWritableDb();

        // encrypted SQLCipher database
        // note: you need to add SQLCipher to your dependencies, check the build.gradle file
        // DaoMaster.DevOpenHelper helper = new DaoMaster.DevOpenHelper(this, "notes-db-encrypted");
        // Database db = helper.getEncryptedWritableDb("encryption-key");

        this.daoSession = new DaoMaster(db).newSession();
    }

    /**
     * @return .
     */
    public DaoSession getDaoSession() {
        return this.daoSession;
    }
}
