package jp.co.kyon.ideabox.business.usecases;

import android.content.Context;
import android.util.Log;
import android.view.View;

import jp.co.kyon.ideabox.R;
import jp.co.kyon.ideabox.business.entities.greendao.BaseEntity;
import jp.co.kyon.ideabox.data.storage.service.IdeaService;
import jp.co.kyon.ideabox.presenters.activities.MainActivity;

/**
 * フラグメントのイベント処理を管理します.
 * <p>
 * Created by t_okeya on 2016/09/01.
 */
public enum FragmentEvent implements Event {

    /**
     * イベント処理です.
     */
    Event1 {
        public void apply(Context context, View view) {

            Log.d(String.valueOf(Log.DEBUG), "button id : " + String.valueOf(view.getId()));

            IdeaService service = new IdeaService(context);

            switch (view.getId()) {
                case R.id.add_button:
                    break;
                default:
                    break;
            }
        }
    };

}
