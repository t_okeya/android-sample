package jp.co.kyon.ideabox.presenters.activities;

import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v4.view.PagerTabStrip;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.view.Gravity;
import android.view.View;

import java.util.ArrayList;
import java.util.List;

import jp.co.kyon.ideabox.R;
import jp.co.kyon.ideabox.business.usecases.CallbackListener;
import jp.co.kyon.ideabox.business.usecases.Event;
import jp.co.kyon.ideabox.presenters.adapters.TabsPagerAdapter;
import jp.co.kyon.ideabox.presenters.fragments.AbstractFragment;
import jp.co.kyon.ideabox.presenters.fragments.IB001Fragment;
import jp.co.kyon.ideabox.presenters.fragments.IB002Fragment;
import jp.co.kyon.ideabox.presenters.views.widgets.IBLinearLayout;

public class MainActivity extends AppCompatActivity implements CallbackListener {

    public void submit(Event event, View view) {
        event.apply(this, view);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // main layout
        IBLinearLayout main = new IBLinearLayout(this);
        main.setOrientation(IBLinearLayout.VERTICAL);

        // viewPager
        ViewPager viewPager = new ViewPager(getApplicationContext());
        viewPager.setId(R.id.view_pager);
        main.addView(viewPager, new IBLinearLayout.LayoutParams(
                IBLinearLayout.LayoutParams.MATCH_PARENT, IBLinearLayout.LayoutParams.MATCH_PARENT));

        // pagerAdapter
        TabsPagerAdapter pagerAdapter = new TabsPagerAdapter(getSupportFragmentManager(), viewPager.getId());
        pagerAdapter.addFragments(this.getFragments(), this.getTitles());
        viewPager.setAdapter(pagerAdapter);

        // pagerTabStrip
        PagerTabStrip pagerTabStrip = new PagerTabStrip(getApplicationContext());
        pagerTabStrip.setId(R.id.pager_tab_strip);
        pagerTabStrip.setGravity(Gravity.TOP);
        pagerTabStrip.setTextColor(Color.BLACK);
        pagerTabStrip.setPadding(0, 20, 0, 0);
        viewPager.addView(pagerTabStrip, new IBLinearLayout.LayoutParams(
                IBLinearLayout.LayoutParams.MATCH_PARENT, IBLinearLayout.LayoutParams.WRAP_CONTENT));

        setContentView(main, new IBLinearLayout.LayoutParams(
                IBLinearLayout.LayoutParams.MATCH_PARENT, IBLinearLayout.LayoutParams.MATCH_PARENT));
    }

    private List<AbstractFragment> getFragments() {
        List<AbstractFragment> fragments = new ArrayList<>();
        fragments.add(IB001Fragment.getInstance(new String[]{"IB001"}));
        fragments.add(IB002Fragment.getInstance(new String[]{"IB002"}));
        fragments.add(IB001Fragment.getInstance(new String[]{"IB003"}));
        fragments.add(IB001Fragment.getInstance(new String[]{"IB004"}));
        return fragments;
    }

    private List<String> getTitles() {
        List<String> titles = new ArrayList<>();
        titles.add("list");
        titles.add("add");
        titles.add("other");
        titles.add("etc");
        return titles;
    }

    /**
     * 戻るボタン押下時に呼び出されます.
     */
    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if (0 < fm.getBackStackEntryCount()) {
            fm.popBackStack();
            return;
        }
        finish();
    }

}
