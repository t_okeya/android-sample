package jp.co.kyon.ideabox.business.usecases;

import android.content.Context;
import android.view.View;

/**
 * イベント処理を管理する抽象クラスです.
 * <p>
 * Created by t_okeya on 2016/09/01.
 */
public interface Event {

    /**
     * イベント処理を行います.
     *
     * @param context {@link Context}
     * @param view    {@link View}
     */
    public void apply(Context context, View view);

}
