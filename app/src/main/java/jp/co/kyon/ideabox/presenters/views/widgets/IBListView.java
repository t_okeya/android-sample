package jp.co.kyon.ideabox.presenters.views.widgets;

import android.content.Context;
import android.widget.ListView;

/**
 * <code>android.widget.ListView</code>拡張クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IBListView extends ListView {

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBListView(Context context) {
        super(context);
    }

}
