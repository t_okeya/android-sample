package jp.co.kyon.ideabox.business.entities.greendao;

import org.greenrobot.greendao.annotation.Entity;
import org.greenrobot.greendao.annotation.Index;
import org.greenrobot.greendao.annotation.NotNull;
import org.greenrobot.greendao.annotation.Generated;

/**
 * アイデア<code>Entity</code>クラスです.
 * <p>
 * Created by t_okeya on 2016/09/01.
 */
@Entity(
        indexes = { @Index(value = "idea1, tag1 desc", unique = true) },
        generateConstructors = true,
        generateGettersSetters = true
)
public class IdeaEntity extends BaseEntity {

    @NotNull
    public String idea1;
    public String idea2;
    public String idea3;
    @NotNull
    public String tag1;
    public String tag2;
    public String tag3;

    /**
     * コンストラクタです.
     */
    public IdeaEntity() {
        super();
    }

@Generated(hash = 523100554)
public IdeaEntity(@NotNull String idea1, String idea2, String idea3,
        @NotNull String tag1, String tag2, String tag3) {
    this.idea1 = idea1;
    this.idea2 = idea2;
    this.idea3 = idea3;
    this.tag1 = tag1;
    this.tag2 = tag2;
    this.tag3 = tag3;
}

public String getIdea1() {
    return this.idea1;
}

public void setIdea1(String idea1) {
    this.idea1 = idea1;
}

public String getIdea2() {
    return this.idea2;
}

public void setIdea2(String idea2) {
    this.idea2 = idea2;
}

public String getIdea3() {
    return this.idea3;
}

public void setIdea3(String idea3) {
    this.idea3 = idea3;
}

public String getTag1() {
    return this.tag1;
}

public void setTag1(String tag1) {
    this.tag1 = tag1;
}

public String getTag2() {
    return this.tag2;
}

public void setTag2(String tag2) {
    this.tag2 = tag2;
}

public String getTag3() {
    return this.tag3;
}

public void setTag3(String tag3) {
    this.tag3 = tag3;
}
}
