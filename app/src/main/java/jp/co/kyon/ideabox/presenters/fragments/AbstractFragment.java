package jp.co.kyon.ideabox.presenters.fragments;

import android.content.Context;
import android.graphics.Typeface;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import jp.co.kyon.ideabox.business.entities.greendao.BaseEntity;
import jp.co.kyon.ideabox.business.usecases.CallbackListener;

/**
 * フラグメントの抽象クラスです.
 * 各画面のフラグメントは当クラスを継承してください.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public abstract class AbstractFragment extends Fragment {

    /** コールバックリスナー {@link CallbackListener}. */
    public CallbackListener listener = null;

    /** フォント {@link Typeface}. */
    private Typeface face = null;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        this.listener = (CallbackListener) context;
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        outState.putAll(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup viewGroup, Bundle savedInstanceState) {
        this.face = Typeface.createFromAsset(getActivity().getBaseContext().getAssets(), "fonts/azuki.ttf");
        return this.setLayout();
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
    }

    @Override
    public void onStart() {
        super.onStart();
    }

    @Override
    public void onResume() {
        super.onResume();
    }

    @Override
    public void onStop() {
        super.onStop();
    }

    @Override
    public void onPause() {
        super.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public abstract View setLayout();

    public abstract void updateLayout();

    public abstract BaseEntity getEntity();

    public Typeface getFace() {
        return this.face;
    }

}
