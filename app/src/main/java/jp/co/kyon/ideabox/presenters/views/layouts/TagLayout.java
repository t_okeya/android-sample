package jp.co.kyon.ideabox.presenters.views.layouts;

import android.content.Context;
import android.graphics.Typeface;

import com.rengwuxian.materialedittext.MaterialEditText;

import jp.co.kyon.ideabox.presenters.views.widgets.IBEditText;
import jp.co.kyon.ideabox.presenters.views.widgets.IBLinearLayout;

/**
 * <code>TagLayout</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class TagLayout extends IBLinearLayout {

    private IBEditText tagEditText = null;

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public TagLayout(Context context) {
        super(context);
    }

    public void init(int text, int hint, int editTextId) {

        this.tagEditText = new IBEditText(getTarget());
        this.tagEditText.setLayoutParams(
                new IBLinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        this.tagEditText.setId(editTextId);
        this.tagEditText.setHint(hint);
        this.tagEditText.setFloatingLabel(MaterialEditText.FLOATING_LABEL_HIGHLIGHT);
        this.tagEditText.setFloatingLabelText(getContext().getResources().getText(text));
        super.addView(this.tagEditText);
    }

    public void setFont(Typeface face) {
        this.tagEditText.setTypeface(face);
    }

    public String getText4EditText() {
        return this.tagEditText.getText().toString();
    }

    public void setText4EditText(String text) {
        this.tagEditText.setText(text);
    }

    public void clearText4EditText() {
        this.tagEditText.setText("");
    }
}
