package jp.co.kyon.ideabox.presenters.adapters;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import java.util.List;

import jp.co.kyon.ideabox.presenters.views.layouts.ListLayout;

/**
 * <code>ListAdapter</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IBListAdapter extends BaseAdapter {

    private Activity target = null;

    private List<ApplicationInfo> lists = null;

    public IBListAdapter(Context context, List<ApplicationInfo> lists) {
        super();
        this.target = (Activity) context;
        this.lists = lists;
    }

    @Override
    public int getCount() {
        return lists.size();
    }

    @Override
    public ApplicationInfo getItem(int position) {
        return lists.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public int getViewTypeCount() {
        // menu type count
        return 3;
    }

    @Override
    public int getItemViewType(int position) {
        // current menu type
        return position % 3;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if (convertView == null) {
            ListLayout listLayout = new ListLayout(this.target);
            listLayout.init(Integer.parseInt("12345"), "hoge");
            convertView = listLayout;
        }
        ApplicationInfo info = this.getItem(position);
        ((ListLayout) convertView).setText(info.loadLabel(this.target.getPackageManager()));

        return convertView;
    }
}
