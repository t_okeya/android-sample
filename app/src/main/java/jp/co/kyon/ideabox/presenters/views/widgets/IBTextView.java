package jp.co.kyon.ideabox.presenters.views.widgets;

import android.content.Context;
import android.widget.TextView;

/**
 * <code>android.widget.TextView</code>拡張クラスです.
 *
 * Created by t_okeya on 2016/09/01.
 */
public class IBTextView extends TextView {

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBTextView(Context context) {
        super(context);
    }

}
