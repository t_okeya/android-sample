package jp.co.kyon.ideabox.presenters.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentTransaction;

import java.util.ArrayList;
import java.util.List;

import jp.co.kyon.ideabox.presenters.fragments.AbstractFragment;

/**
 * <code>TabsPagerAdapter</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class TabsPagerAdapter extends FragmentPagerAdapter {

    /**
     * フラグメント配列 {@link List<AbstractFragment>}.
     */
    private List<AbstractFragment> fragments = new ArrayList<>();
    /**
     * タイトル配列 {@link List<CharSequence>}.
     */
    private List<String> titles = new ArrayList<>();
    /**
     * フラグメントマネージャ {@link FragmentManager}.
     */
    private FragmentManager fm = null;
    /**
     * フラグメントトランザクション {@link FragmentTransaction}.
     */
    private FragmentTransaction ft = null;
    /**
     * ページID {@link int}.
     */
    private int pagerId = 0;

    public TabsPagerAdapter(FragmentManager fm, int pagerId) {
        super(fm);
        this.fm = fm;
        this.pagerId = pagerId;
    }

    /**
     * 指定位置のフラグメントを返却します.
     *
     * @param position {@link int}
     * @return フラグメント
     */
    @Override
    public Fragment getItem(int position) {
        return this.fragments.get(position);
    }

    /**
     * フラグメント件数を返却します.
     *
     * @return フラグメント件数
     */
    @Override
    public int getCount() {
        return this.fragments.size();
    }

    /**
     * フラグメント位置を返却します.
     *
     * @param object {@link Object}
     * @return フラグメント位置
     */
    @Override
    public int getItemPosition(Object object) {
        return POSITION_NONE;
    }

    /**
     * 指定位置のフラグメントのページ名を返却します.
     *
     * @param position {@link int}
     * @return ページ名
     */
    @Override
    public CharSequence getPageTitle(int position) {
        return (CharSequence) this.titles.get(position);
    }

    /**
     * フラグメントの内容が変更されたことを通知します.
     */
    @Override
    public void notifyDataSetChanged() {
        if (this.ft != null) {
            this.ft.commitAllowingStateLoss();
            this.ft = null;
        }
        super.notifyDataSetChanged();
    }

    /**
     * フラグメント配列、タイトル名配列を追加します.
     *
     * @param fragments {@link List<AbstractFragment>}
     * @param titles    {@link List<String>}
     */
    public void addFragments(List<AbstractFragment> fragments, List<String> titles) {
        this.fragments.addAll(fragments);
        this.titles.addAll(titles);
    }

    /**
     * ページを切り替えます.
     *
     * @param position {@link int}
     * @param fragment {@link AbstractFragment}
     */
    public void replace(int position, AbstractFragment fragment) {
        String tag = this.makeFragmentName(this.pagerId, position);
        if (this.ft == null) {
            this.ft = this.fm.beginTransaction();
        }
        this.ft.replace(this.pagerId, fragment, tag);
//        this.ft.addToBackStack(null);
        this.fragments.set(position, fragment);
    }

    /**
     * フラグメント名を生成します.
     *
     * @param viewId {@link int}
     * @param id     {@link int}
     * @return フラグメント名
     */
    private String makeFragmentName(int viewId, int id) {
        return "android:switcher:" + viewId + ":" + id;
    }

}
