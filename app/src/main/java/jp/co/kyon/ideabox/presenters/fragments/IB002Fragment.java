package jp.co.kyon.ideabox.presenters.fragments;

import android.os.Bundle;
import android.view.View;

import jp.co.kyon.ideabox.R;
import jp.co.kyon.ideabox.business.entities.greendao.BaseEntity;
import jp.co.kyon.ideabox.business.entities.greendao.IdeaEntity;
import jp.co.kyon.ideabox.business.usecases.FragmentEvent;
import jp.co.kyon.ideabox.presenters.views.layouts.ButtonLayout;
import jp.co.kyon.ideabox.presenters.views.layouts.IdeaLayout;
import jp.co.kyon.ideabox.presenters.views.layouts.TagLayout;
import jp.co.kyon.ideabox.presenters.views.widgets.IBLinearLayout;
import jp.co.kyon.ideabox.presenters.views.widgets.IBScrollView;

/**
 * <code>IB002Fragment</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IB002Fragment extends AbstractFragment {

    /**
     * アイデア１ {@link IdeaLayout}.
     */
    private IdeaLayout idea1 = null;

    /**
     * アイデア２ {@link IdeaLayout}.
     */
    private IdeaLayout idea2 = null;

    /**
     * アイデア３ {@link IdeaLayout}.
     */
    private IdeaLayout idea3 = null;

    /**
     * タグ１ {@link TagLayout}.
     */
    private TagLayout tag1 = null;

    /**
     * タグ２ {@link TagLayout}.
     */
    private TagLayout tag2 = null;

    /**
     * タグ３ {@link TagLayout}.
     */
    private TagLayout tag3 = null;

    /**
     * 登録ボタン {@link ButtonLayout}.
     */
    private ButtonLayout addButton = null;

    /**
     * 登録ボタン押下時のリスナー.
     */
    private View.OnClickListener onClickListener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            listener.submit(FragmentEvent.Event1, view);
        }
    };

    /**
     * インスタンスを返却します.
     *
     * @param args {@link String[]}
     * @return {@link IB002Fragment}
     */
    public static IB002Fragment getInstance(String[] args) {

        IB002Fragment fragment = new IB002Fragment();

        Bundle bundle = new Bundle();
        for (int i = 0; i < args.length; i++) {
            bundle.putString(String.valueOf(i), args[i]);
        }
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View setLayout() {

        // main
        IBLinearLayout main = new IBLinearLayout(getActivity());
        main.setOrientation(IBLinearLayout.VERTICAL);

        // tab area
        IBLinearLayout tabArea = new IBLinearLayout(getActivity());
        tabArea.setOrientation(IBLinearLayout.VERTICAL);
        main.addView(tabArea, new IBLinearLayout.LayoutParams(IBLinearLayout.LayoutParams.MATCH_PARENT, 0, 1.f));

        // scrollView
        IBScrollView scrollView = new IBScrollView(getActivity());
        main.addView(scrollView, new IBLinearLayout.LayoutParams(IBLinearLayout.LayoutParams.MATCH_PARENT, 0, 6.f));

        // contents
        IBLinearLayout contents = new IBLinearLayout(getActivity());
        contents.setOrientation(IBLinearLayout.VERTICAL);
        scrollView.addView(contents, new IBLinearLayout.LayoutParams(
                IBLinearLayout.LayoutParams.MATCH_PARENT, IBLinearLayout.LayoutParams.MATCH_PARENT));

        // common layoutParams
        IBLinearLayout.LayoutParams itemParams = new IBLinearLayout.LayoutParams(
                IBLinearLayout.LayoutParams.MATCH_PARENT, 0, 1.f);

        // idea1
        this.idea1 = new IdeaLayout(getActivity());
        this.idea1.init(R.string.idea_text_view_1, R.string.idea_edit_text_1, R.id.idea_text_edit_1);
        this.idea1.setFont(getFace());
        this.idea1.setPadding(20, 50, 20, 10);
        contents.addView(this.idea1, itemParams);

        // idea2
        this.idea2 = new IdeaLayout(getActivity());
        this.idea2.init(R.string.idea_text_view_2, R.string.idea_edit_text_2, R.id.idea_text_edit_2);
        this.idea2.setFont(getFace());
        this.idea2.setPadding(20, 50, 20, 10);
        contents.addView(this.idea2, itemParams);

        // idea3
        this.idea3 = new IdeaLayout(getActivity());
        this.idea3.init(R.string.idea_text_view_3, R.string.idea_edit_text_3, R.id.idea_text_edit_3);
        this.idea3.setFont(getFace());
        this.idea3.setPadding(20, 50, 20, 10);
        contents.addView(this.idea3, itemParams);

        // tag1
        this.tag1 = new TagLayout(getActivity());
        this.tag1.init(R.string.tag_text_view_1, R.string.tag_edit_text_1, R.id.tag_text_edit_1);
        this.tag1.setFont(getFace());
        this.tag1.setPadding(20, 50, 20, 10);
        contents.addView(this.tag1, itemParams);

        // tag2
        this.tag2 = new TagLayout(getActivity());
        this.tag2.init(R.string.tag_text_view_2, R.string.tag_edit_text_2, R.id.tag_text_edit_2);
        this.tag2.setFont(getFace());
        this.tag2.setPadding(20, 50, 20, 10);
        contents.addView(this.tag2, itemParams);

        // tag3
        this.tag3 = new TagLayout(getActivity());
        this.tag3.init(R.string.tag_text_view_3, R.string.tag_edit_text_3, R.id.tag_text_edit_3);
        this.tag3.setFont(getFace());
        this.tag3.setPadding(20, 50, 20, 10);
        contents.addView(this.tag3, itemParams);

        // put button
        this.addButton = new ButtonLayout(getActivity());
        this.addButton.init(R.string.add_button, R.id.add_button);
        this.addButton.setFont(getFace());
        this.addButton.setPadding(20, 50, 20, 10);
        this.addButton.setClickListener(this.onClickListener);
        main.addView(this.addButton, new IBLinearLayout.LayoutParams(IBLinearLayout.LayoutParams.MATCH_PARENT, 0, 1.f));

        return main;
    }

    @Override
    public void updateLayout() {

    }

    @Override
    public BaseEntity getEntity() {
        IdeaEntity entity = new IdeaEntity();
        entity.idea1 = null;
        return entity;
    }

}
