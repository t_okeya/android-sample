package jp.co.kyon.ideabox.presenters.views.widgets;

import android.content.Context;
import android.widget.Button;

/**
 * <code>android.widget.Button</code>拡張クラスです.
 *
 * Created by t_okeya on 2016/09/01.
 */
public class IBButton extends Button {

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBButton(Context context) {
        super(context);
    }

}
