package jp.co.kyon.ideabox.presenters.fragments;

import android.os.Bundle;
import android.util.TypedValue;
import android.view.View;
import android.widget.ListView;

import jp.co.kyon.ideabox.business.entities.greendao.BaseEntity;
import jp.co.kyon.ideabox.presenters.adapters.IBListAdapter;
import jp.co.kyon.ideabox.presenters.views.widgets.IBLinearLayout;

/**
 * <code>IB001Fragment</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IB001Fragment extends AbstractFragment {

    /**
     * インスタンスを返却します.
     *
     * @param args {@link String[]}
     * @return {@link IB001Fragment}
     */
    public static IB001Fragment getInstance(String[] args) {

        IB001Fragment fragment = new IB001Fragment();

        Bundle bundle = new Bundle();
        for (int i = 0; i < args.length; i++) {
            bundle.putString(String.valueOf(i), args[i]);
        }
        fragment.setArguments(bundle);

        return fragment;
    }

    @Override
    public View setLayout() {

        IBLinearLayout main = new IBLinearLayout(getActivity());
        main.setOrientation(IBLinearLayout.VERTICAL);
        main.setLayoutParams(new IBLinearLayout.LayoutParams(
                IBLinearLayout.LayoutParams.MATCH_PARENT, IBLinearLayout.LayoutParams.MATCH_PARENT));

        IBLinearLayout tabArea = new IBLinearLayout(getActivity());
        tabArea.setOrientation(IBLinearLayout.VERTICAL);
        main.addView(tabArea, new IBLinearLayout.LayoutParams(IBLinearLayout.LayoutParams.MATCH_PARENT, 0, 1.f));

        ListView listView = new ListView(getActivity());
        listView.setId(Integer.parseInt("999999"));
        listView.setDivider(null);
        main.addView(listView, new IBLinearLayout.LayoutParams(IBLinearLayout.LayoutParams.MATCH_PARENT, 0, 7.f));

        IBListAdapter adapter = new IBListAdapter(getActivity(), getActivity().getPackageManager().getInstalledApplications(0));
        listView.setAdapter(adapter);

        return main;
    }

    @Override
    public void updateLayout() {

    }

    @Override
    public BaseEntity getEntity() {
        return null;
    }

    private int dp2px(int dp) {
        return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, getResources().getDisplayMetrics());
    }
}