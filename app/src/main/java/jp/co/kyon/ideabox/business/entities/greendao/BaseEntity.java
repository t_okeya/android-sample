package jp.co.kyon.ideabox.business.entities.greendao;

import org.greenrobot.greendao.annotation.Id;
import org.greenrobot.greendao.annotation.NotNull;

import java.util.Date;

/**
 * <code>Entity</code>の基底クラスです.
 * <p>
 * Created by t_okeya on 2016/09/01.
 */
public abstract class BaseEntity {

    /**
     * ID.
     */
    @Id
    public Integer id;

    /**
     * 作成日.
     */
    @NotNull
    public Date createdAt;

    /**
     * 更新日.
     */
    @NotNull
    public Date updatedAt;

    /**
     * 削除フラグ.
     */
    @NotNull
    public String deletedFlg;

    /**
     * バージョンNo.
     */
    @NotNull
    public Integer versionNo;

}
