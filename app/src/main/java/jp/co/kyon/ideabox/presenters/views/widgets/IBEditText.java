package jp.co.kyon.ideabox.presenters.views.widgets;

import android.content.Context;

import com.rengwuxian.materialedittext.MaterialEditText;

/**
 * <code>com.rengwuxian.materialedittext.MaterialEditText</code>拡張クラスです.
 *
 * Created by t_okeya on 2016/09/01.
 */
public class IBEditText extends MaterialEditText {

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBEditText(Context context) {
        super(context);
    }

}
