package jp.co.kyon.ideabox.presenters.views.widgets;

import android.content.Context;
import android.widget.ScrollView;

/**
 * <code>android.widget.ScrollView</code>拡張クラスです.
 *
 * Created by t_okeya on 2016/09/01.
 */
public class IBScrollView extends ScrollView {

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBScrollView(Context context) {
        super(context);
    }
}
