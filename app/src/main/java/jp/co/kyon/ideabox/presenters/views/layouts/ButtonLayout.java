package jp.co.kyon.ideabox.presenters.views.layouts;

import android.content.Context;
import android.graphics.Typeface;
import android.view.View;

import jp.co.kyon.ideabox.presenters.views.widgets.IBButton;
import jp.co.kyon.ideabox.presenters.views.widgets.IBLinearLayout;

/**
 * <code>ButtonLayout</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class ButtonLayout extends IBLinearLayout {

    private IBButton button;

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public ButtonLayout(Context context) {
        super(context);
        super.setOrientation(IBLinearLayout.HORIZONTAL);
    }

    public void init(int text, int id) {

        this.button = new IBButton(getTarget());
        this.button.setLayoutParams(
                new IBLinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, LayoutParams.WRAP_CONTENT));
        this.button.setId(id);
        this.button.setText(text);
        super.addView(button);
    }

    public void setFont(Typeface face) {
        this.button.setTypeface(face);
    }

    public void setClickListener(View.OnClickListener clickListener) {
        this.button.setOnClickListener(clickListener);
    }

}
