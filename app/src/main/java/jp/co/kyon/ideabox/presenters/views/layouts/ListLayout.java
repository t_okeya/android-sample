package jp.co.kyon.ideabox.presenters.views.layouts;

import android.content.Context;

import jp.co.kyon.ideabox.presenters.views.widgets.IBLinearLayout;
import jp.co.kyon.ideabox.presenters.views.widgets.IBTextView;

/**
 * <code>ListMenuLayout</code>クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class ListLayout extends IBLinearLayout {

    private IBTextView textView = null;

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public ListLayout(Context context) {
        super(context);
    }

    public void init(int textViewId, String text) {

        this.textView = new IBTextView(getTarget());
        this.textView.setId(textViewId);
        this.textView.setLayoutParams(new IBLinearLayout.LayoutParams(LayoutParams.MATCH_PARENT, 150));
        this.textView.setText(text);
        super.addView(this.textView);
    }

    public void setText(CharSequence text) {
        this.textView.setText(text);
    }

}
