package jp.co.kyon.ideabox.business.usecases;

import android.view.View;

/**
 * コールバックリスナーです.
 * <p>
 * Created by t_okeya on 2016/09/01.
 */
public interface CallbackListener {

    /**
     * フラグメントイベント発生時のリスナーです.
     *
     * @param event {@link Event}
     * @param view  {@link View}
     */
    void submit(Event event, View view);
}
