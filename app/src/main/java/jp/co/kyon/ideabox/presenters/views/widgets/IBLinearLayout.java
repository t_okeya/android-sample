package jp.co.kyon.ideabox.presenters.views.widgets;

import android.app.Activity;
import android.content.Context;
import android.widget.LinearLayout;

/**
 * <code>android.widget.LinearLayout</code>拡張クラスです.
 * <p/>
 * Created by t_okeya on 2016/09/01.
 */
public class IBLinearLayout extends LinearLayout {

    /** 対象の{@link Activity}. */
    private Activity target = null;

    /**
     * コンストラクタです.
     *
     * @param context {@link Context}
     */
    public IBLinearLayout(Context context) {
        super(context);
        this.target = (Activity) context;
    }

    /**
     * 対象の{@link Activity}を返却します.
     *
     * @return {@link Activity}
     */
    public Activity getTarget() {
        return this.target;
    }

}
